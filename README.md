# Sisu calendar simplifier

Telegram bot and ICS manipulating microservice to get only the fields you're interested into your calendar.

## Architecture

Tg bot @sisucalbot
-> Webhook to TG Lambda
   Takes Sisu calendar link and converts it to a link directed to the ICS microservice
-> User adds link to calendar and ICS microservice is invoked.
   .ics fetched from sisu (id stored in URL params) and based on URL flags some fields are removed
   Modified .ics is returned to the calendar app.

This is deployed to AWS Lambda as 2 lambdas using Docker container based on distroless ARM Python 3.10.
Code is updated by `UpdateFunctionCode` Lambda API call containing the wanted container digest.
A local image can be deployed by (TODO add proper commands):
```
docker build ./tg -t sisu-tg-lambda
docker push sisu-tg-lambda
aws lambda update-function-code --container-digest ...
```

There's `.gitlab-ci.yml` that automates this for pushed commits on `main` branch.

