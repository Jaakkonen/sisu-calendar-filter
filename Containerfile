ARG APP=calendar_filter
ARG PY_VER=3.10
ARG FUNCTION_DIR=/app

ARG BASE=docker.io/python:${PY_VER}-alpine
# == Poetry build ==
# Installs poetry, and installs requirements from poetry.lock/pyproject.toml
# to /site-packages.
# In final image $PYTHONHOME/site-packages can be populated with contents of
# /site-packages.
FROM ${BASE} AS poetry-depencency-env
RUN apk add --no-cache gcc musl-dev libffi-dev
RUN pip install poetry
COPY pyproject.toml poetry.lock /
RUN poetry export \
    | pip install --target=/site-packages -r /dev/stdin \
    && rm pyproject.toml poetry.lock \
    && rm -r /site-packages/*.dist-info


# == AWS Lambda runtime interface build ==
# Installs awslambdaric and its python dependencises to /site-packages.
# Note that libstdc++.so.6 and libgcc_s.so.1 are required
# during runtime.
FROM ${BASE} AS lambda-builder
RUN apk add --no-cache \
    build-base \
    libtool \
    autoconf \
    automake \
    libexecinfo-dev \
    make \
    cmake \
    libcurl \
    && pip install --target=/site-packages awslambdaric \
    && rm -r /site-packages/*.dist-info

# == Minimal python ==
# Removes unnecessary stuff from /usr/local/lib/python3.10/
# and copies required runtime dynamically loaded libraries
# to /pyrunlib.
FROM $BASE AS stripped
RUN cd /usr/local/lib/python3*/ \
    # Easteregg modules
    && rm antigravity.py this.py \
    # tab/whitespace formatting helper
    && rm tabnanny.py \
    # CGI traceback formatter
    && rm cgitb.py \
    # Graphical libraries & webbrowser
    && rm -r turtle.py webbrowser.py curses tkinter idlelib \
    # Dependency helpers and pre-installed modules
    && rm -r site-packages venv ensurepip \
    # Formatting helpers
    && rm -r lib2to3 pydoc.py pydoc_data \
    # Build config, makefile and left-arounds
    && rm -r config-3.*-*-linux-gnu \
    # License
    && rm LICENSE.txt \
    # Doctest (runs samples from docstrings)
    && rm doctest.py \
    # CGI interface
    && rm cgi.py \
    # WAVE and AIFF file support
    && rm wave.py aifc.py \
    # Pickle utils
    && rm pickletools.py \
    # Pickle (Required by many packages, it may be that this needs to be included)
    && rm pickle.py \
    # Distutils (strtobool from distutils.util is often required though)
    && rm -r distutils \
    # Dumb DBM. GDBM is included in libraries and implements NDBM.
    && rm dbm/dumb.py

RUN mkdir /_pyrunlib /pyrunlib \
    && cp /lib/*.so* /usr/lib \
    # Get required libraries from APK virtual package
    && apk info -R .python-rundeps \
    | tail -n+2 \
    | cut -d: -f2 \
    | xargs -I{} mv /usr/lib/{} /pyrunlib

# == AWS lambda runtime interface emulator downloader ==
# Downloads AWS RIE for local development testing and makes it executable.
# Used by copying /aws-lambda-rie to target image and prepending entrypoint
# by the executable path.
FROM docker.io/alpine:latest AS rie-downloader
RUN wget https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie \
    && chmod +x aws-lambda-rie

# == Function image ==
# Copies python executable, libpython and python runtime deps from
# the base image and the minimal python images, and copies
# python modules from aws runtime interface client builder and
# poetry dependency builder.
# Finally sets entrypoint to AWS RIC which will start
# /app/handler.py::handler(ctx, event) when requests come in.
FROM scratch AS _lambda
ARG APP
ARG BASE
ARG PY_VER
ARG FUNCTION_DIR
# Python itself
COPY --from=${BASE} /usr/local/bin/python /bin/
# Python runtime libraries
COPY --from=${BASE} /usr/local/lib/libpython${PY_VER}.so.1.0 /lib/
COPY --from=${BASE} /lib/ld-musl-x86_64.so.1 /lib/
COPY --from=stripped /pyrunlib /lib
COPY --from=stripped /usr/local/lib/python${PY_VER}/ /usr/local/lib/python${PY_VER}/
# awslambdaric libraries.
COPY --from=lambda-builder \
    /usr/lib/libstdc++.so.6 \
    /usr/lib/libgcc_s.so.1 \
    /lib/
# Dependencies
COPY --from=lambda-builder \
    /site-packages \
    /usr/local/lib/python${PY_VER}/site-packages/
COPY --from=poetry-depencency-env \
    /site-packages \
    /usr/local/lib/python${PY_VER}/site-packages/
WORKDIR $FUNCTION_DIR
COPY ${APP}/ .
ENTRYPOINT ["/bin/python", "-m", "awslambdaric"]
CMD ["app.handler"]
# ** Debugging options **
# COPY --from=${BASE} /bin /bin
# ENTRYPOINT ["/bin/sh"]

# == Squashed function image ==
FROM scratch AS lambda
WORKDIR $FUNCTION_DIR
ENTRYPOINT ["/bin/python", "-m", "awslambdaric"]
CMD ["app.handler"]
COPY --from=_lambda / /

# == AWS Runtime interface emulator image ==
# Emulate AWS runtime interface to run the lambda locally.
# `docker build . -t app_rie --target=lambda-rie && docker run -p 8080:8080 app_rie`
# Invoke function with
# `curl -XPOST "http://localhost:8080/2015-03-31/functions/function/invocations" -d '{}'`
FROM _lambda AS lambda-rie
COPY --from=rie-downloader aws-lambda-rie /bin/aws-lambda-rie
COPY --from=alpine /bin /bin
ENTRYPOINT ["/bin/aws-lambda-rie", "/bin/python", "-m", "awslambdaric"]
CMD ["app.handler"]
